package com.epam.tat.matrixprocessor.impl;

import com.epam.tat.matrixprocessor.IMatrixProcessor;
import com.epam.tat.matrixprocessor.exception.MatrixProcessorException;

/**
 * Function Description:
 * Complete the functions below. All methods must work with matrices of the double type.
 * <p>
 * Constraints:
 * 0 < m < 10
 * 0 < n < 10
 * where m - number of rows in matrix
 * where n - number of columns in matrix
 * <p>
 * In case of incorrect input values or inability to perform a calculation, the method should throw an appropriate
 * exception.
 */
public class MatrixProcessor implements IMatrixProcessor {

    /**
     * Matrix transpose is an operation on a matrix where its rows become columns with the same numbers.
     * Ex.:
     * |1 2|			|1 3 5|
     * |3 4|   ====>	|2 4 6|
     * |5 6|
     *
     * @param matrix - matrix for transposition
     * @return the transposed matrix
     */
    @Override
    public double[][] transpose(double[][] matrix) {

        isNullOrEmpty(matrix);

        int m = matrix.length;
        int n = matrix[0].length;
        double[][] transposed = new double[n][m];

        for (int i = 0; i < transposed.length; i++) {
            for (int j = 0; j < transposed[i].length; j++) {
                transposed[i][j] = matrix[j][i];
            }
        }

        return transposed;
    }

    /**
     * The method flips the matrix clockwise.
     * Ex.:
     * * |1 2|			|5 3 1|
     * * |3 4|   ====>	|6 4 2|
     * * |5 6|
     *
     * @param matrix - rotation matrix
     * @return rotated matrix
     */
    @Override
    public double[][] turnClockwise(double[][] matrix) {

        isNullOrEmpty(matrix);

        int m = matrix.length;
        int n = matrix[0].length;
        double[][] rotated = new double[n][m];

        for (int i = 0; i < rotated.length; i++) {
            for (int j = 0; j < rotated[i].length; j++) {
                rotated[i][j] = matrix[rotated[i].length - j - 1][i];
            }
        }

        return rotated;
    }

    /**
     * This method multiplies matrix firstMatrix by matrix secondMatrix
     * <p>
     * See {https://en.wikipedia.org/wiki/Matrix_multiplication}
     *
     * @param firstMatrix  - first matrix to multiply
     * @param secondMatrix - second matrix to multiply
     * @return result matrix
     */
    @Override
    public double[][] multiplyMatrices(double[][] firstMatrix, double[][] secondMatrix) {

        isNullOrEmpty(firstMatrix);
        isNullOrEmpty(secondMatrix);

        if (firstMatrix[0].length != secondMatrix.length) {
            throw new MatrixProcessorException();
        }

        double[][] production = new double[firstMatrix.length][secondMatrix[0].length];

        for (int i = 0; i < production.length; i++) {
            for (int j = 0; j < production[i].length; j++) {
                production[i][j] = roundToThreeDecimalPoint(
                        scalarProduct(firstMatrix, i, secondMatrix, j));
            }
        }

        return production;
    }

    /**
     * This method returns the inverse of the matrix
     * <p>
     * See {https://en.wikipedia.org/wiki/Invertible_matrix}
     *
     * @param matrix - input matrix
     * @return inverse matrix for input matrix
     */
    @Override
    public double[][] getInverseMatrix(double[][] matrix) {

        isNullOrEmpty(matrix);

        if (getMatrixDeterminant(matrix) == 0) {
            throw new MatrixProcessorException();
        }

        return new double[matrix.length][matrix.length];
    }

    /**
     * This method returns the determinant of the matrix
     * <p>
     * See {https://en.wikipedia.org/wiki/Determinant}
     *
     * @param matrix - input matrix
     * @return determinant of input matrix
     */
    @Override
    public double getMatrixDeterminant(double[][] matrix) {

        isNullOrEmpty(matrix);

        if (!isSquareMatrix(matrix)) {
            throw new MatrixProcessorException();
        }

        if (matrix.length == 1) {
            return matrix[0][0];
        }

        if (matrix.length == 2) {
            return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
        }

        double sum = 0;
        double sign = 1;

        for (int i = 0; i < matrix.length; i++) {
            sum += sign * matrix[0][i] * getMatrixDeterminant(additionalMinorByFirstRow(matrix, i));
            sign = -sign;
        }

        return roundToThreeDecimalPoint(sum);
    }

    private double[][] additionalMinorByFirstRow(double[][] matrix, int columnIndexToExclude) {

        double[][] minor = new double[matrix.length - 1][matrix.length - 1];

        for (int i = 1; i < matrix.length; i++) {
            double[] minorRow = new double[minor.length];

            System.arraycopy(matrix[i], 0, minorRow, 0, columnIndexToExclude);
            System.arraycopy(matrix[i], columnIndexToExclude + 1, minorRow, columnIndexToExclude, minorRow.length - columnIndexToExclude);

            minor[i - 1] = minorRow;
        }

        return minor;
    }

    private double roundToThreeDecimalPoint(double number) {
        return Double.parseDouble(String.format("%.3f", number));
    }

    private double scalarProduct(double[][] firstMatrix, int firstMatrixRow,
                                 double[][] secondMatrix, int secondMatrixColumn) {
        double sum = 0;

        for (int i = 0; i < firstMatrix[firstMatrixRow].length; i++) {
            sum += firstMatrix[firstMatrixRow][i] * secondMatrix[i][secondMatrixColumn];
        }

        return sum;
    }

    private boolean isSquareMatrix(double[][] matrix) {
        return matrix.length == matrix[0].length;
    }

    private void isNullOrEmpty(double[][] matrix) {

        if (matrix == null || matrix.length == 0) {
            throw new MatrixProcessorException();
        }

    }

}
